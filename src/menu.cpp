#include "menu.hpp"
#include "menu_estoque.hpp"
#include "menu_venda.hpp"

#include <iostream>
#include <string>
#include <cstdlib>
#include <thread>

using namespace std;

void menu() {

    int opcao = -1;

    while(opcao != 0) {
        
        system("clear");
        
        cout << "" << endl;
        cout << "------------------------Bodega da Vitória------------------------" << endl;
        cout << "Escolha uma opção" << endl;
        cout << "1 - Estoque" << endl;
        cout << "2 - Venda" << endl;
        cout << "3 - Recomendação" << endl;
        cout << "0 - Encerrar Programa" << endl;
        cout << "-----------------------------------------------------------------" << endl;

        cin >> opcao;

        switch (opcao) {
        
        case 1:
            system("clear");
            menu_estoque(); // Função que chama o modo Estoque 
            break;

        case 2:
            system("clear");
            menu_venda(); // Função que chama o modo Venda
            break;

        case 3:
            cout << "Modo Recomendação!" << endl;
            break;

        case 0:
            system("clear");
            cout << "Programa encerrado, até logo!" << endl;
            //std::this_thread::sleep_for(std::chrono::seconds{1}); // Função que congela a tela
            exit(0);
            //break;
        
        default:
            cout << "Entrada inválida, digite uma entrada válida |1|2|3|0|" << endl;           
            break;
        }

    }

}