#include "menu_estoque.hpp"
#include "estoque.hpp"

#include <iostream>

using namespace std;

void menu_estoque() {

    Estoque estoque;

    int opcao_estoque = -1;


    while(opcao_estoque != 0) {
        
        //system("clear");

        cout << "" << endl;
        cout << "=============================Estoque=============================" << endl;       
        cout << "Escolha uma opção" << endl;
        cout << "1 - Cadastrar Novos Produtos" << endl;
        cout << "2 - Conferir Estoque" << endl;
        cout << "0 - Sair do Modo estoque" << endl;                
        cout << "=================================================================" << endl;

        
        cin >> opcao_estoque;

        switch (opcao_estoque) {
    
        case 1:
            cout << "Cadastrar Novos Produtos" << endl;
            estoque.cadastrar_produto();
            break;
        case 2:
            cout << "Conferir Estoque" << endl;
            estoque.imprime_estoque();
            cout << "" << endl;

            break;
        case 0:
            break;
        
        default:
            cout << "Entrada inválida, digite uma entrada válida |1|2|0|" << endl;           
            break;
        }
    }

}