#include "menu_venda.hpp"
#include "carrinho.hpp"
#include "cliente.hpp"
#include "funcionario.hpp"
#include "socio.hpp"
#include "estoque.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <thread>

using namespace std;

void menu_venda() {

    Funcionario funcionario;
    Cliente cliente;
    Socio socio;
    Carrinho venda;
    Estoque estoque;
     
    int opcao_venda = -2;

    string nome_funcionario;
    
    string nome_cliente;
    long int cpf_cliente;
    string email_cliente;

    string nome_socio;
    long int cpf_socio;
    
    
    cout << "Qual funcionário está usando o sistema [primeiro ou último nome] ? ";
    
    cin >> nome_funcionario;
    funcionario.set_nome(nome_funcionario);
    

    system("clear");
    
    while(opcao_venda != 0) {

        cout << "" << endl;
        cout << "..............................Venda.............................." << endl;
    
        cout << "" << endl;    
        cout << "         Ok " << funcionario.get_nome() << " o que deseja fazer?" << endl;

        cout << "" << endl;    
        cout << "1 - Verificar Cadastro de Cliente" << endl;
        cout << "2 - Cadastrar Cliente" << endl;
        cout << "3 - Verificar Sociedade" << endl;
        cout << "4 - Cadastrar Sócio" << endl;
        cout << "5 - Iniciar venda" << endl;
        cout << "0 - Sair do Modo Venda" << endl;
        cout << "................................................................." << endl;
        
        cin >> opcao_venda;

        system("clear");

        switch(opcao_venda) {
                
            // Verficia cadastro de clientes    
            
            case 1: 
                
                cliente.verificar();
                
                break;
            

            // Cadastra Cliente

            case 2:
                
                cout << "" << endl;
                cout << "Digite o primeiro e último nome do cliente [Nome_Cliente]: ";
                cin >> nome_cliente;

                cout << "Digite o cpf do cliente: ";
                cin >> cpf_cliente;

                cout << "Digite o email do cliente: ";
                cin >> email_cliente;

                cliente.cadastrar(nome_cliente, cpf_cliente, email_cliente);
                break;

            // Verifica sócio
            
            case 3: 
                
                int retorno; // inteiro para o retorno do metodo verificar()
                retorno = socio.verificar();
                
                if(retorno == 1) {
                    cout << "Cliente já possui sociedade com a loja!" << endl;
                    std::this_thread::sleep_for(std::chrono::seconds{1}); // Função que congela a tela
                    system("clear");
                }

                else {
                    cout << "Cliente não possui sociedade com a loja!" << endl;
                    cout << "Deseja se associar? Socios possuem 15% de desconto nas compras!" << endl;
                    std::this_thread::sleep_for(std::chrono::seconds{2}); // Função que congela a tela
                    system("clear");
                }

                break;
            
            // Cadastra sócio

            case 4:
                        
                cout << "" << endl;
                cout << "Digite o primeiro e último nome do cliente [Nome_Cliente]: ";
                cin >> nome_socio;

                cout << "Digite o cpf do cliente: ";
                cin >> cpf_socio;

                socio.cadastrar(nome_socio, cpf_socio);

                break;
            
            
            // Inicia o caixa

            case 5:
                
                estoque.imprime_estoque();
                cout << "" << endl;
                venda.caixa();
                remove("doc/carrinho.txt");

                break;

            // Sai do Modo Venda

            case 0:
                
                exit(0);

            // Caso seja inserido opções inválidas
            
            default:
                
                cout << "Entrada inválida, digite uma entrada válida |1|2|3|4|5|0|" << endl;
                break;

        }
    }
}