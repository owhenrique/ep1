#include "funcionario.hpp"
#include <iostream>
#include <string>

using namespace std;

Funcionario::Funcionario() {
    set_nome(nome);
}

Funcionario::~Funcionario() {

}

void Funcionario::set_nome(string nome) {
    this -> nome = nome;
}

string Funcionario::get_nome() {
    return nome;
}