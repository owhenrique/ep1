#include "cliente.hpp"

#include <iostream>
#include <string>
#include <string.h>
#include <iomanip>
#include <thread>
#include <cstdlib>
#include <fstream>

using namespace std;

Cliente::Cliente() {
    set_nome("unknow");
    set_cpf(00000000000);
}

Cliente::~Cliente() {

}

void Cliente::set_nome(string nome) {
    this -> nome = nome;
}

string Cliente::get_nome() {
    return nome;
}

void Cliente::set_cpf(long int cpf) {
    this -> cpf = cpf;
}

long int Cliente::get_cpf() {
    return cpf;
}

void Cliente::set_email(string email) {
    this -> email = email;
}

string Cliente::get_email() {
    return email;
}

void Cliente::verificar() {

    FILE * arquivo;

    char nome_cliente_lido[50];
    char cpf_cliente[50];
    char cpf_cliente_lido[50];
    char email_lido[50];

    arquivo = fopen("doc/cliente.txt", "r");

    cout << "Digite o cpf do cliente: ";
    cin >> cpf_cliente;
    
    system("clear");

    while(! feof(arquivo)) {

        fscanf(arquivo, "%s %s %s\n", &nome_cliente_lido, &cpf_cliente_lido, &email_lido);

        if(strcmp(cpf_cliente_lido, cpf_cliente)) {
            
            cout << "Cliente já possui cadastro com a loja!" << endl;
            std::this_thread::sleep_for(std::chrono::seconds{1}); // Função que congela a tela
            system("clear");
        }

        else { 
    
            cout << "Cliente não possui cadastro com a loja!" << endl;
            std::this_thread::sleep_for(std::chrono::seconds{1}); // Função que congela a tela
            system("clear");
        }
    }

    fclose(arquivo);
}

void Cliente::cadastrar(string nome, long int cpf, string email) {
    
    ofstream arquivo;
    
    arquivo.open("doc/cliente.txt", ios::app);

    if(arquivo.is_open()) {

        arquivo << nome;
        arquivo << " ";
        arquivo << cpf;
        arquivo << " ";
        arquivo << email << endl;

    }

    else {
        cout << "Não foi possível abrir o Banco de Clientes." << endl;
    }

    arquivo.close();

    system("clear");
}