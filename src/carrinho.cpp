#include "carrinho.hpp"
#include "estoque.hpp"

#include <iostream>
#include <string>
#include <string.h>
#include <fstream>
#include <thread>

using namespace std;

Carrinho::Carrinho(){

}

Carrinho::~Carrinho(){
    
}

void Carrinho::caixa() {

    int opcao_carrinho = 1;


    while(opcao_carrinho != 0){
        
        system("clear");

        cout << "Caixa" << endl;
        
        adicionar_produto();

        cout << "" << endl;
        cout << "Deseja inserir mais produtos? [1 - Sim | 0 - Não]" << endl;

        cin >> opcao_carrinho;

    }

    system("clear");

    finaliza_compra();
}

void Carrinho::adicionar_produto() {

    ofstream arquivo;
    char categoria_produto_carrinho[50];
    string produto_carrinho;
    int quantidade_carrinho;

    arquivo.open("doc/carrinho.txt", ios::app);

    system("clear");
    cout << "Digite a categoria do produto [roupa, perfumaria, calçado]: ";
    cin >> categoria_produto_carrinho;

    if(!(strcmp(categoria_produto_carrinho, "roupa") == 0) || !(strcmp(categoria_produto_carrinho, "perfumaria") == 0) || !(strcmp(categoria_produto_carrinho, "calçado") == 0)) {

        system("clear");
        cout << "Entrada inválida, digite novamente [roupa, perfumaria, calçado]: ";
        cin >> categoria_produto_carrinho;
    }

    cout << "Digite o nome do produto a ser inserido no Carrinho [nome_do_produto]: ";
    cin >> produto_carrinho;
    
    cout << "Quantas unidades? ";
    cin >> quantidade_carrinho;

    arquivo << categoria_produto_carrinho;
    arquivo << " ";
    arquivo << produto_carrinho;
    arquivo << " ";
    arquivo << quantidade_carrinho << endl;

}

void Carrinho::finaliza_compra() {

    FILE * carrinho;
    FILE * estoque_roupa;
    FILE * estoque_prfm;
    FILE * estoque_clcd;

    char categoria_carrinho[50];
    char produto_carrinho[50];
    int quantidade_carrinho;
    
    char produto_estoque[50];
    int quantidade_estoque;
    float valor_estoque;
    
    float valor_compra = 0.0;

    carrinho = fopen("doc/carrinho.txt", "r");

    while(! feof(carrinho)) {

        fscanf(carrinho, "%s %s %i\n", categoria_carrinho, produto_carrinho, &quantidade_carrinho);

        if(strcmp("roupa", categoria_carrinho) == 0) {
            
            estoque_roupa = fopen("doc/estoque_roupa.txt",  "r");

            fscanf(estoque_roupa, "%i %s %f\n", &quantidade_estoque, produto_estoque, &valor_estoque);

                if(strcmp(produto_estoque, produto_carrinho) == 0) {
                    

                    if(quantidade_carrinho > quantidade_estoque) {

                        cout << "O estoque possuí apenas " << quantidade_estoque << " unidades. Deseja quantas peças? " << endl;
                        cin >> quantidade_carrinho;

                    }

                    cout << "Produto " << produto_carrinho << " adicionado ao carrinho." << endl;
                    valor_compra = valor_compra + (valor_estoque * quantidade_carrinho);

                }

            fclose(estoque_roupa);

        }

        if(strcmp("perfumaria", categoria_carrinho) == 0) {
            
            estoque_prfm = fopen("doc/estoque_prfm.txt",  "r");

            fscanf(estoque_roupa, "%i %s %f\n", &quantidade_estoque, produto_estoque, &valor_estoque);

            if(strcmp(produto_estoque, produto_carrinho) == 0) {
                    

                if(quantidade_carrinho > quantidade_estoque) {

                    cout << "O estoque possuí apenas " << quantidade_estoque << " unidades. Deseja quantas peças? " << endl;
                    cin >> quantidade_carrinho;

                }

            
            cout << "Produto " << produto_carrinho << " adicionado ao carrinho." << endl;
            valor_compra = valor_compra + (valor_estoque * quantidade_carrinho);

            }

            fclose(estoque_prfm);

        }

        if(strcmp("calçado", categoria_carrinho) == 0) {

            estoque_clcd = fopen("doc/estoque_clcd.txt",  "r");
            
            fscanf(estoque_roupa, "%i %s %f\n", &quantidade_estoque, produto_estoque, &valor_estoque);

            if(strcmp(produto_estoque, produto_carrinho) == 0) {
                    

                if(quantidade_carrinho > quantidade_estoque) {

                    cout << "O estoque possuí apenas " << quantidade_estoque << " unidades. Deseja quantas peças? " << endl;
                    cin >> quantidade_carrinho;

                }

            cout << "Produto " << produto_carrinho << " adicionado ao carrinho." << endl;
            valor_compra = valor_compra + (valor_estoque * quantidade_carrinho);

            }           

            fclose(estoque_clcd); 
        }

    }
    
    cout << "" << endl;
    cout << "O total da compra foi de: " << valor_compra << endl;
    cout << "O valor da compra para clientes sócios é: " << (valor_compra - (valor_compra * 0.15)) << endl;
    std::this_thread::sleep_for(std::chrono::seconds{5});
    
    system("clear");

    remove("doc/carrinho.txt");
    
    fclose(carrinho);

}