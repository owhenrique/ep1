#include "estoque.hpp"

#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <thread>

using namespace std;

void Estoque::set_quantidade(int quantidade) {
    this -> quantidade = quantidade;
}

int Estoque::get_quantidade() {
    return quantidade;
}

void Estoque::set_produto(string produto) {
    this -> produto = produto;
}

string Estoque::get_produto() {
    return produto;
}

void Estoque::set_valor(float) {
    this -> valor = valor;
}

float Estoque::get_valor() {
    return valor;
}

void Estoque::cadastrar_produto() {

    int opcao_cat = -1;
    
    system("clear");

    ofstream arquivo;
    string linha;

    cout << "Deseja cadastrar um produto de que Categoria?" << endl;
    cout << "1 - Roupa" << endl;
    cout << "2 - Perfumaria" << endl;
    cout << "3 - Calçado" << endl;
    cout << "0 - Sair do Cadastro de Produtos" << endl;

    cin >> opcao_cat;

    switch (opcao_cat) {
    
    case 1:
        
        arquivo.open("doc/estoque_roupa.txt", ios::app);

        if(arquivo.is_open()) {

            system("clear");

            cout << "----------------Cadastro de novo produto----------------" << endl;
            cout << " " << endl;
            cout << "Digite o quantidade em estoque: ";
            cin >> this -> quantidade;
            arquivo << quantidade;
            
            cout << "" << endl;

            cout << "Digite o nome do produto dessa forma [nome_do_produto]: ";
            cin >> this -> produto;
            arquivo << " "; 
            arquivo << produto;

            cout << "" << endl;


            cout << "Digite o valor do produto: ";
            cin >> this -> valor;
            arquivo << " ";
            arquivo << valor << endl;
        
        } else {
            cout << "Erro. Não foi possível abrir o estoque de roupas!" << endl;
    }   
    
        system("clear");

        break;
    
    case 2:
        
        arquivo.open("doc/estoque_prfm.txt", ios::app);

        if(arquivo.is_open()) {

            system("clear");

            cout << "----------------Cadastro de novo produto----------------" << endl;
            cout << "" << endl;
            cout << "Digite o quantidade em estoque: ";
            cin >> this -> quantidade;
            arquivo << quantidade;
            
            cout << "" << endl;

            cout << "Digite o nome do produto dessa forma [nome_do_produto]: ";
            cin >> this -> produto;
            arquivo << " "; 
            arquivo << produto;

            cout << "" << endl;


            cout << "Digite o valor do produto: ";
            cin >> this -> valor;
            arquivo << " ";
            arquivo << valor << endl;
        
        } else {
            cout << "Erro. Não foi possível abrir o estoque de perfumaria!" << endl;
    }
        
        system("clear");

        break;
    
    case 3:
        
        arquivo.open("doc/estoque_clcd.txt", ios::app);

        if(arquivo.is_open()) {

            system("clear");

            cout << "----------------Cadastro de novo produto----------------" << endl;
            cout << "" << endl;
            cout << "Digite o quantidade em estoque: ";
            cin >> this -> quantidade;
            arquivo << quantidade;
            
            cout << "" << endl;

            cout << "Digite o nome do produto dessa forma [nome_do_produto]: ";
            cin >> this -> produto;
            arquivo << " "; 
            arquivo << produto;

            cout << "" << endl;


            cout << "Digite o valor do produto: ";
            cin >> this -> valor;
            arquivo << " ";
            arquivo << valor << endl;
        
        } else {
            cout << "Erro. Não foi possível abrir o estoque de calçados!" << endl;
    }

        system("clear");

        break;
    
    case 0:
        
        break;

    default:
        
        cout << "Entrada inválida, digite uma entrada válida |1|2|3|0|" << endl;
        
        break;
    }
    
    arquivo.close();
}

void Estoque::imprime_estoque() {
    
    system("clear");

    
    ifstream arquivo;
    string linha;
    
    arquivo.open("doc/estoque_roupa.txt");

    if(arquivo.is_open()) {
        
        cout << "" << endl;
        cout << "======================Estoque de Roupas======================" << endl;
        cout << "" << endl;
        cout << "         Qntd. em estoque| Nome do produto | Valor       " << endl;
        cout << "" << endl;

        while(getline(arquivo, linha)) {
            cout << linha << endl;
        }
    }

    else {
        cout << "Erro. Não foi possível abrir o estoque de roupas" << endl;
    }

    arquivo.close();

    arquivo.open("doc/estoque_prfm.txt");

    if(arquivo.is_open()) {
        
        cout << "" << endl;
        cout << "====================Estoque de Perfumaria====================" << endl;
        cout << "" << endl;
        cout << "            Qntd. em estoque| Nome do produto | Valor       " << endl;
        cout << "" << endl;

        while(getline(arquivo, linha)) {
            cout << linha << endl;
        }
    }

    else {
        cout << "Erro. Não foi possível abrir o estoque de roupas" << endl;
    }

    arquivo.close();

    arquivo.open("doc/estoque_clcd.txt");

    if(arquivo.is_open()) {
        
        cout << "" << endl;
        cout << "=====================Estoque de Calçados=====================" << endl;
        cout << "" << endl;
        cout << "          Qntd. em estoque| Nome do produto | Valor       " << endl;
        cout << "" << endl;

        while(getline(arquivo, linha)) {
            cout << linha << endl;
        }
        
    }



    else {
        cout << "Erro. Não foi possível abrir o estoque de roupas" << endl;
    }

    arquivo.close();

    cout << "" << endl;
    cout << "O estoque estará na tela por 15 segundos....................." << endl;
    cout << "=============================================================" << endl;

    std::this_thread::sleep_for(std::chrono::seconds{15}); // Função que congela a tela

    system("clear");
}
