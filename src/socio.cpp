#include "socio.hpp"

#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <string.h>

using namespace std;

void Socio::set_nome(string nome) {
    this -> nome = nome;
}

string Socio::get_nome() {
    return nome;
}

void Socio::set_cpf(long int cpf) {
    this -> cpf = cpf;
}

long int Socio::get_cpf() {
    return cpf;
}

int Socio::verificar() {

    FILE * arquivo;
    //char nome_socio[50];
    char nome_socio_lido[50];    
    char cpf_socio[50];
    char cpf_socio_lido[50];


    arquivo = fopen("doc/socio.txt", "r");

    cout << "Digite o cpf do cliente: ";
    cin >> cpf_socio;

    system("clear");

    while(! feof(arquivo)) {
        
        fscanf(arquivo, "%s %s\n", nome_socio_lido, cpf_socio_lido);
                   
        if(strcmp(cpf_socio_lido, cpf_socio)) {    
            return 1;
        }

        else {
            return 0;
        }  
    }

    fclose(arquivo);

}

void Socio::cadastrar(string nome, long int cpf) {

    ofstream arquivo;
    
    arquivo.open("doc/socio.txt", ios::app);

    if(arquivo.is_open()) {

        arquivo << nome;
        arquivo << " ";
        arquivo << cpf << endl;

    }

    else {
        cout << "Não foi possível abrir o Banco de Clientes Sócios." << endl;
    }

    arquivo.close();

    system("clear");
}