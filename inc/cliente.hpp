#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

using namespace std;

class Cliente {
    
    protected:

        string nome;
        long int cpf;
        string email;


    public:

    Cliente();
    ~Cliente();
    
    void set_nome(string nome);
    string get_nome();

    void set_cpf(long int cpf);
    long int get_cpf();
    
    void set_email(string email);
    string get_email();

    void verificar();
    void cadastrar(string nome, long int cpf, string email);

};


#endif