#ifndef FUNCIONARIO_HPP
#define FUNCIONARIO_HPP

#include "cliente.hpp"

using namespace std;

class Funcionario: public Cliente {

    public:

        Funcionario();
        ~Funcionario();
        
        void set_nome(string nome);
        string get_nome();
};

#endif