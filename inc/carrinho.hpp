#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <string>

using namespace std;

class Carrinho {

     public:

          Carrinho();
          ~Carrinho();

          void caixa();
          void adicionar_produto();
          void finaliza_compra();
};

#endif 