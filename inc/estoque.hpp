#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include <string>

using namespace std;

class Estoque {

    public:

        int quantidade;
        string produto;
        float valor;


        void set_quantidade(int quantidade);
        int get_quantidade();

        void set_produto(string produto);
        string get_produto();

        void set_valor(float valor);
        float get_valor();
        
        void cadastrar_produto();
        void imprime_estoque();


};

#endif