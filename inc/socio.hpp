#ifndef SOCIO_HPP
#define SOCIO_HPP

#include "cliente.hpp"
#include <string>

using namespace std;

class Socio: public Cliente {

    public:

        void set_nome(string nome);
        string get_nome();

        void set_cpf(long int cpf);
        long int get_cpf();

        int verificar();
        void cadastrar(string nome, long int cpf);

};


#endif